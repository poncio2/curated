const plugin = require('tailwindcss/plugin')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: ['Inconsolata', 'sans-serif'],
      mono: ['Space Mono', 'monospace'],
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    plugin(function({ addBase, theme }) {
      addBase({
        'html': { backgroundColor: theme('colors.black') },
      })
    })
  ],
}
